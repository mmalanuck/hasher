﻿using Hasher.model;
using Hasher.viewmodel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hasher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IApplicationViewModel _vm;

        public MainWindow()
        {
            InitializeComponent();
            _vm = new ApplicationViewModel();
            DataContext = _vm;
        }

        private void AddFile_Clicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == false)
                return;
            var files = openFileDialog.FileNames;
            _vm.AddSources(files);
        }

        private void AddFolder_Clicked(object sender, RoutedEventArgs e)
        {
            Gat.Controls.OpenDialogView openDialog = new Gat.Controls.OpenDialogView();
            Gat.Controls.OpenDialogViewModel vm = (Gat.Controls.OpenDialogViewModel)openDialog.DataContext;
            vm.IsDirectoryChooser = true;
            vm.Show();

            var folder = vm.SelectedFilePath;
            _vm.AddSourceFolder(folder);
        }

        private void RemoveSource_Clicked(object sender, RoutedEventArgs e)
        {
            var items = SourcesList.SelectedItems.OfType<BaseSource>().ToList();
            _vm.RemoveSources(items);
            Console.WriteLine(items);
        }
    }
}

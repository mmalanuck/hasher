﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Hasher
{
    public class Icons
    {
        static Canvas MakeIcon(string data)
        {
            Viewbox res = new Viewbox();
            var canvas = new Canvas() { Width = 24, Height = 24 };
            var path = new Path() { Data = Geometry.Parse(data), Fill = Brushes.Black };
            canvas.Children.Add(path);
            return canvas;
        }

        public Canvas File { get; set; } = MakeIcon("M13,9V3.5L18.5,9M6,2C4.89,2 4,2.89 4,4V20A2,2 0 0,0 6,22H18A2,2 0 0,0 20,20V8L14,2H6Z");
    }
}

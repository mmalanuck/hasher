﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace Hasher.md
{
    class Button: ButtonBase 
    {
        public static readonly DependencyProperty IsRaisedProperty =
            DependencyProperty.Register("IsRaised", typeof(bool), typeof(Button));

        public static readonly DependencyProperty RippleCenterProperty =
            DependencyProperty.Register("RippleCenter", typeof(Point), typeof(Button));

        public bool IsRaised
        {
            get { return (bool) GetValue(IsRaisedProperty); }
            set { SetValue(IsRaisedProperty, value); }
        }

        public Point RippleCenter
        {
            get { return (Point)GetValue(RippleCenterProperty); }
            set { SetValue(RippleCenterProperty, value); }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            
            UIElement container = VisualTreeHelper.GetParent(this) as UIElement;
            if (container == null)
                return;
            var parent = VisualTreeHelper.GetParent(container) as UIElement;
            if (parent == null)
                return;

            var localPos = e.GetPosition(this);
            var pts = this.PointToScreen(localPos);
            //var offset = new Point(16, 0);            
            Console.WriteLine(parent.ToString());
            var offset = this.PointToScreen(new Point(0,0));
            var res = Point.Subtract(pts, offset);
            RippleCenter = (Point) res;
            Console.WriteLine("============");
            Console.WriteLine(pts.ToString());
            Console.WriteLine(offset.ToString());
            Console.WriteLine(localPos.ToString());
            
        }
        //protected override void OnMouseMove(MouseEventArgs e)
        //{
        //    base.OnMouseMove(e);
        //    RippleCenter = e.GetPosition(this);
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hasher.model;

namespace Hasher.viewmodel
{
    public class ApplicationViewModel : Notifier, IApplicationViewModel
    {
        private IApplicationModel _model;
        private ObservableCollection<BaseSource> _sources;
        
        public ApplicationViewModel(IApplicationModel model)
        {            
            if (model == null)
                model = new ApplicationModel();

            _model = model;
            _sources = _model.Sources;
        }
        public ApplicationViewModel() : this(null) { }

        public ObservableCollection<BaseSource> Sources{
            get { return _sources; }
            set
            {
                _sources = value;
                this.NotifyPropertyChanged("Sources");
            }
        }

        public void AddSources(string[] files)
        {
            _model.AddSources(files);
            
        }

        public void AddSourceFolder(string folder)
        {
            _model.AddSource(folder);
        }

        public void RemoveSources(List<BaseSource> items)
        {
            _model.RemoveSources(items);
        }
    }
}

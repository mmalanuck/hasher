﻿using Hasher.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasher.viewmodel
{
    public interface IApplicationViewModel
    {
        ObservableCollection<BaseSource> Sources { get; set; }

        void AddSources(string[] files);
        void AddSourceFolder(string folder);
        void RemoveSources(List<BaseSource> items);
    }
}

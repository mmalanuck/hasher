﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasher.model
{
    public class ApplicationModel : IApplicationModel
    {
        public ObservableCollection<BaseSource> Sources { get; set; } = new ObservableCollection<BaseSource>();
        public ApplicationModel()
        {
        }

        public void AddSources(string[] files)
        {
            foreach (var file in files)
            {
                Sources.Add(new BaseSource() { Path = file });
            }
        }

        public void AddSource(string path)
        {
            Sources.Add(new BaseSource() { Path = path });
        }

        public void RemoveSources(List<BaseSource> items)
        {
            foreach(var item in items)
            {
                Sources.Remove(item);
            }
        }
    }
}

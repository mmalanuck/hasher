﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hasher.model
{
    public interface IApplicationModel
    {
        ObservableCollection<BaseSource> Sources { get; set; }

        void AddSources(string[] files);
        void AddSource(string folder);
        void RemoveSources(List<BaseSource> items);
    }
}
